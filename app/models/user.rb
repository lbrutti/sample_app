class User < ActiveRecord::Base
  I18n.enforce_available_locales = true
  #avendo creato un indice con vincolo di unicita' sul
  #campo mail, dobbiamo uniformarne il formato in minuscolo. 
  #(non tutti i db supportano unicita' case insensitive)
  before_save { self.mail = mail.downcase }
  
  
  
  #validazione sulla presenza e la lunghezza dell'attributo :name
  validates :name, presence: true, length: {maximum: 50}
  #validazione sulla mail
  #definizio regex per indirizzo mail valido
  VALID_MAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :mail,  presence: true, 
                    format: { with: VALID_MAIL_REGEX},
                    uniqueness: { case_sensitive: false }
  #la validazione di unicita' deve essere riportata anche come vincolo 
  #nel database (si crea un indice sulla mail)
  
  #validazioni pwd
  #password
  has_secure_password
  validates :password, length: { minimum: 6 }

end
