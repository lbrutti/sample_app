class UsersController < ApplicationController
  def show
    #@user è una variabile d'istanza visibile anche alla view
    @user = User.find(params[:id])
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      #variabile flash per mostrare un messaggio esclusivo del 
      #metodo create
      flash[:success] = "Benvenuto! #{@user.name}"
      redirect_to @user
    else
      render 'new'
    end
  end
  
  private
  #filtra tutti e soli i parametri necessari alla creazione dell'utente
  def user_params
    #:user e' il simbolo usato come chiave dell'hash dei paramentri
    params.require(:user).permit(:name, :mail, :password,
     :password_confirmation)
  end
end
