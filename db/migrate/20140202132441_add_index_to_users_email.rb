class AddIndexToUsersEmail < ActiveRecord::Migration
  def change
    #add_index e' un metodo di Rails per aggiungere un indice alla
    #tabella passata come argomento.
    add_index :users, :mail, unique: true
  end
end
