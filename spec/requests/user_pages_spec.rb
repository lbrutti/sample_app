require 'spec_helper'

describe "User pages" do

  subject { page }
  let(:submit) { "Create my account" }

  
#test per pagina di sign up
  describe "signup page" do
    before { visit signup_path }
    
    it { should have_content('Sign Up') }
    it { should have_title(full_title('Sign Up')) }
    
    describe "posting invalid form data" do
      it "should not create a new user" do 
        expect { click_button submit }.not_to change(User, :count) 
      end
      
    end
    
    describe "post valid data and create a user" do
      before {
        fill_in "Name",         with:"Example User"
        fill_in "Mail",         with: "user@example.com"
        fill_in "Password",     with: "foobar"
        fill_in "Confirmation", with: "foobar"
      }
      
      it "should create a new user" do
        expect click_button submit
      end
      
      describe "after saving the user" do
        before { click_button submit }
        let(:user) { User.find_by(mail: "user@example.com")}
        it { should have_content "Benvenuto"}
        it { should have_title user.name}
      end
    end
  end
  
  #test per messaggio di errore
  describe "error message display" do
      before { 
        visit signup_path
        click_button submit 
      }
      it { should have_content('Error') }
  end
    
    
#test per user page
  describe "profile page" do
    let(:user) { FactoryGirl.create(:user) }
    before { visit user_path(user) }
  
    it { should have_content(user.name) }
    it { should have_title(user.name) }
  end
  
end