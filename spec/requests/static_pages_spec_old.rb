#VECCHIO FILE DI TEST CON CODICE NON OTTIMIZZATO. VEDERE NUOVO PER OTTIMIZZAZIONI

require 'spec_helper'

describe "StaticPages" do
  #definizione della variabile per la stringa di titolo
  let(:base_title) {"Rails Tutorial Sample App"}
  
# test della homepage
  describe "Home page" do
    it "should have content 'Sample App'" do
      visit '/static_pages/home'
      expect(page).to have_content("Sample App")
    end
    
    it "should have the title 'Home'" do
      visit '/static_pages/home'
      expect(page).to have_title("#{base_title} | Home")
    end
  end

  # test dell'help page
  describe "Help page" do
    it "should have content 'Help Page'" do
      visit '/static_pages/help'
      expect(page).to have_content('Help Page')
    end
    
    it "should have the title 'Help'" do
      visit '/static_pages/help'
      expect(page).to have_title("#{base_title} | Help")
    end
    
  end # end home test

  # test dell'about page
  describe "About page" do
    it "should have content 'About Us'" do
      visit '/static_pages/about'
      expect(page).to have_content('About Us')
    end
    
        it "should have the title 'About Us'" do
      visit '/static_pages/about'
      expect(page).to have_title("#{base_title} | About Us")
    end
  end
  
   # test della contact page
  describe "Contact page" do
    it "should have content 'Contact Us'" do
      visit '/static_pages/contact'
      expect(page).to have_content('Contact Us')
    end
    
        it "should have the title 'Contact Us'" do
      visit '/static_pages/contact'
      expect(page).to have_title("#{base_title} | Contact Us")
    end
  end
  
end
