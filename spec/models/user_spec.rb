# Test per il modello User
require 'spec_helper'

describe User do

#before indica le azioni da compiere prima di eseguire i test
#in questo caso procedo alla creazione  di un nuovo utente con compilati
#i campi nome e mail
  before {

  #definito come variabile di istanza, in questo modo viene creata una sola
  #volta ed utilizzata nei vari test
    @user = User.new(name: "user1", mail: "user@example.com",
    password: "foobar",
    password_confirmation: "foobar")
  }

  #una volta creato l'utente di test,  lo pongo come oggetto di test
  subject{ @user }

  #i test riguardano il comportamento dell'oggetto.
  #deve rispondere alla richiesta di nome e mail
  it { should respond_to(:name) }
  it { should respond_to(:mail) }
  it { should respond_to(:password_digest) }

  #questi non sono salvati nel db, sono temporanei
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }

  it { should respond_to(:authenticate) }

  #test sulla validazione, per farlo fallire, va commentata la
  #validazione nella classe User
  #la riga qui sotto fa riferimento all'utente valido creato nel before
  #iniziale
  it {should be_valid}

  #test per l'utente non valido (avendo commentato la validazione,
  #fallisce)
  #test sul nome
  describe "when name is not present (user non valido, in teoria)" do
  #nuovo before invalidare utente creato inizialmente
    before { @user.name="" }
    it {should_not be_valid}
  end

  describe "nome troppo lungo (user non valido, in teoria)" do
    before { @user.name='a'*51 }
    it {should_not be_valid}
  end

  #test sulla mail
  describe "when mail is not present (user non valido, in teoria)" do
    before { @user.mail="" }
    it {should_not be_valid}
  end
  #test sul formato dell'indirizzo mail
  describe "formato indirizzo mail non valido" do
    it "should be invalid" do
      indirizzi = %w[user@mail  user_at_foo.org user@.it]
      indirizzi.each { |indirizzo_non_valido|
        @user.mail=indirizzo_non_valido
        expect(@user).not_to be_valid
      }
    end
  end

  describe "formato indirizzo mail valido" do
    it "should be valid" do
      indirizzi = %w[user@mail.it  user@foo.org user@tin.it]
      indirizzi.each { |indirizzo_valido|
        @user.mail=indirizzo_valido
        expect(@user).to be_valid
      }
    end
  end

  describe "when mail is already taken" do
    before do
      user_with_same_mail = @user.dup
      #test della mail duplicata ma maiuscola
      user_with_same_mail.mail = @user.mail.upcase
      user_with_same_mail.save
    end

    it {should_not be_valid}
  end
  
  
############################################################
#################Test sulla password########################
############################################################
  describe "when password is not present" do
    before do
      @user = User.new(name: "Example User", mail: "user@example.com",
                     password: " ", password_confirmation: " ")
    end
    it { should_not be_valid }
  end

  describe "when password doesn't match confirmation" do
    before { @user.password_confirmation = "mismatch" }
    it { should_not be_valid }
  end


#presume validazione sulla lunghezza della pwd
  describe "with a password that's too short" do
    before { @user.password = @user.password_confirmation = "a" * 5 }
    it { should be_invalid }
  end
#check sul metodo authenticate
  describe "return value of authenticate method" do
    before { @user.save }
    let(:found_user) { User.find_by(mail: @user.mail) }

    describe "with valid password" do
      it { should eq found_user.authenticate(@user.password) }
    end

    describe "with invalid password" do
      let(:user_for_invalid_password) { found_user.authenticate("invalid") }

      it { should_not eq user_for_invalid_password }
      specify { expect(user_for_invalid_password).to be_false }
    end
  end

end
